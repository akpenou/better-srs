import { getUsers } from "../../helpers/_airtable/user";
import { getCards } from "../../helpers/_airtable/card";
import { getSections } from "../../helpers/_airtable/section";
import { getCourses } from "../../helpers/_airtable/course";
import { getEvents } from "../../helpers/_airtable/event";
import { getNotions } from "../../helpers/_airtable/notion";

const { sequelize } = require("../models");

const { syncUser } = require("./users");
const { syncCard } = require("./cards");
const { syncCourse } = require("./courses");
const { syncSection } = require("./sections");
const { syncEvent } = require("./events");
const { syncNotion } = require("./notions");

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const factory = async () => {
  await sequelize.sync().then(() => {
    console.log(`Database & tables created!`);
  });

  const database = {};

  database.course = await getCourses()
    .then((courses) => {
      return Promise.all(courses.map((course) => syncCourse(course)));
    })
    .catch((err) => console.log(err));

  console.log("courses done!");

  await sleep(1000);

  database.section = await getSections()
    .then((sections) => {
      return Promise.all(sections.map((section) => syncSection(section)));
    })
    .catch((err) => console.log(err));

  console.log("sections done!");

  await sleep(1000);

  database.user = await getUsers()
    .then((users) => {
      return Promise.all(users.map((user) => syncUser(user)));
    })
    .catch((err) => console.log(err));

  console.log("users done!");

  await sleep(1000);

  database.card = await getCards()
    .then((cards) => {
      return Promise.all(cards.map((card) => syncCard(card)));
    })
    .catch((err) => console.log(err));

  console.log("cards done!");
};

if (require.main === module) {
  factory().then(() => {
    console.log("Database sync");
    process.exit();
  });
}

module.exports = factory;
