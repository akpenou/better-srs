const { Users } = require("../models");

async function createUser({ id, name, code, mail, EXP, LEVEL, avatar }) {
  const user = await Users.create({
    airtable_id: id,
    name,
    code,
    mail,
    experience: EXP,
    level: LEVEL,
    avatar,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return user;
}

async function syncUser({ id, name, code, mail, EXP, LEVEL, avatar }) {
  const user = await Users.update(
    {
      name,
      code,
      mail,
      avatar,
    },
    { where: { airtable_id: id } }
  )
    .then(([nbOfUpdatedElements]) => {
      console.log({ nbOfUpdatedElements });
      if (nbOfUpdatedElements) {
        return;
      }

      return createUser({ id, name, code, mail, EXP, LEVEL, avatar });
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return user;
}

module.exports.createUser = createUser;
module.exports.syncUser = syncUser;
