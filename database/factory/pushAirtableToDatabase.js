import { getUsers } from "../../helpers/_airtable/user";
import { getCards } from "../../helpers/_airtable/card";
import { getSections } from "../../helpers/_airtable/section";
import { getCourses } from "../../helpers/_airtable/course";
import { getEvents } from "../../helpers/_airtable/event";
import { getNotions } from "../../helpers/_airtable/notion";

const { sequelize } = require("../models");

const { createUser } = require("./users");
const { createCard } = require("./cards");
const { createCourse } = require("./courses");
const { createSection } = require("./sections");
const { createEvent } = require("./events");
const { createNotion } = require("./notions");

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const factory = async () => {
  await sequelize.sync({ force: true }).then(() => {
    console.log(`Database & tables created!`);
  });

  const database = {};

  database.course = await getCourses()
    .then((courses) => {
      return Promise.all(courses.map((course) => createCourse(course)));
    })
    .catch((err) => console.log(err));

  console.log("courses done!");

  await sleep(1000);

  database.section = await getSections()
    .then((sections) => {
      return Promise.all(sections.map((section) => createSection(section)));
    })
    .catch((err) => console.log(err));

  console.log("sections done!");

  await sleep(1000);

  database.user = await getUsers()
    .then((users) => {
      return Promise.all(users.map((user) => createUser(user)));
    })
    .catch((err) => console.log(err));

  console.log("users done!");

  await sleep(1000);

  database.card = await getCards()
    .then((cards) => {
      return Promise.all(cards.map((card) => createCard(card)));
    })
    .catch((err) => console.log(err));

  console.log("cards done!");

  await sleep(1000);

  database.event = await getEvents()
    .then((events) => {
      return Promise.all(events.map((event) => createEvent(event)));
    })
    .catch((err) => console.log(err));

  console.log("events done!");

  await sleep(1000);

  database.notion = await getNotions()
    .then((notions) => {
      return Promise.all(notions.map((notion) => createNotion(notion)));
    })
    .catch((err) => console.log(err));

  console.log("notions done!");

  await sleep(1000);
};

if (require.main === module) {
  factory().then(() => {
    console.log("Database sync");
    process.exit();
  });
}

module.exports = factory;
