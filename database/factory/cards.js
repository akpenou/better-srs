const { Cards, Sections } = require("../models");

async function getSectionFromAirtableId(sectionId) {
  return Sections.findOne({
    where: {
      airtable_id: sectionId,
    },
  });
}

async function createCard({ id, question, answer, section_id, ...props }) {
  const section = await getSectionFromAirtableId(section_id && section_id[0]);

  const cards = await Cards.create({
    airtable_id: id,
    question,
    answer,
    sectionId: section.id,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return cards;
}

async function syncCard({ id, question, answer, section_id, ...props }) {
  const section = await getSectionFromAirtableId(section_id && section_id[0]);

  const cards = await Cards.update(
    {
      question,
      answer,
      sectionId: section.id,
    },
    { where: { airtable_id: id } }
  )
    .then(([nbOfUpdatedElements]) => {
      console.log({ nbOfUpdatedElements });
      if (nbOfUpdatedElements) {
        return;
      }

      return createCard({ id, question, answer, section_id });
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return cards;
}

module.exports.createCard = createCard;
module.exports.syncCard = syncCard;
