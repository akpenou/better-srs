const { Courses } = require("../models");

async function createCourse({ id, name, description, ...props }) {
  const cards = await Courses.create({
    airtable_id: id,
    name,
    description,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return cards;
}

async function syncCourse({ id, name, description, ...props }) {
  console.log({ id, name, description, ...props });
  const cards = await Courses.update(
    {
      name,
      description,
    },
    { where: { airtable_id: id }, returning: true }
  )
    .then(([nbOfUpdatedElements]) => {
      console.log({ nbOfUpdatedElements });
      if (nbOfUpdatedElements) {
        return;
      }

      return createCourse({ id, name, description });
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return cards;
}

module.exports.createCourse = createCourse;
module.exports.syncCourse = syncCourse;
