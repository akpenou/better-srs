const { Sections, Courses } = require("../models");

async function createSection({
  id,
  name,
  description,
  course: courseId,
  ...props
}) {
  console.log({ name, description, courseId });

  const course = courseId
    ? await Courses.findOne({
        where: { airtable_id: courseId[0] },
      })
    : {};

  const section = await Sections.create({
    airtable_id: id,
    name,
    description,
    courseId: course && course.id,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return section;
}

async function syncSection({
  id,
  name,
  description,
  course: courseId,
  ...props
}) {
  const course = courseId
    ? await Courses.findOne({
        where: { airtable_id: courseId[0] },
      })
    : {};

  console.log({ course });

  const section = await Sections.update(
    {
      name,
      description,
      courseId: course && course.id,
    },
    { where: { airtable_id: id } }
  )
    .then(([nbOfUpdatedElements]) => {
      console.log({ nbOfUpdatedElements });
      if (nbOfUpdatedElements) {
        return;
      }

      return createSection({
        id,
        name,
        description,
        course: courseId,
      });
    })
    .catch((err) => {
      console.log({ id, name, description, course });

      console.log(err);
      return null;
    });

  return section;
}

module.exports.createSection = createSection;
module.exports.syncSection = syncSection;
