const { Events, Users, Cards } = require("../models");

const getEventMeta = async (userId, cardId) => {
  const [user, card] = await Promise.all([
    Users.findOne({
      where: {
        airtable_id: userId,
      },
    }),
    Cards.findOne({
      where: {
        airtable_id: cardId,
      },
    }),
  ]);

  return { user, card };
};

async function createEvent({
  id,
  knowledge,
  experience,
  user: user_id,
  card: card_id,
  ...props
}) {
  console.log({ id, knowledge, experience, user_id, card_id, props });

  const { user, card } = await getEventMeta(user_id[0], card_id[0]);

  const event = await Events.create({
    airtable_id: id,
    knowledge,
    experience,
    cardId: card.id,
    userId: user.id,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return event;
}

async function syncEvent({
  id,
  knowledge,
  experience,
  user: user_id,
  card: card_id,
  ...props
}) {
  const { user, card } = await getEventMeta(user_id[0], card_id[0]);

  const event = await Events.update(
    {
      knowledge,
      experience,
      cardId: card.id,
      userId: user.id,
    },
    { where: { airtable_id: id } }
  )
    .then(([nbOfUpdatedElements]) => {
      console.log({ nbOfUpdatedElements });
      if (nbOfUpdatedElements) {
        return;
      }

      return createEvent({
        id,
        knowledge,
        experience,
        user: user_id,
        card: card_id,
      });
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return event;
}

module.exports.createEvent = createEvent;
module.exports.syncEvent = syncEvent;
