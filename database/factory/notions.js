const { Notions, Users, Cards } = require("../models");

const getNotionMeta = async (userId, cardId) => {
  const [user, card] = await Promise.all([
    Users.findOne({
      where: {
        airtable_id: userId,
      },
    }),
    Cards.findOne({
      where: {
        airtable_id: cardId,
      },
    }),
  ]);

  return { user, card };
};

async function createNotion({
  id,
  stage,
  repetitions,
  "last review": lastReview,
  "next review": nextReview,
  user: user_id,
  card: card_id,
  ...props
}) {
  const { user, card } = await getNotionMeta(user_id[0], card_id[0]);

  const notion = await Notions.create({
    airtable_id: id,
    stage,
    repetitions,
    lastReview,
    nextReview,
    cardId: card.id,
    userId: user.id,
  }).catch((err) => {
    console.log(err);
    return null;
  });

  return notion;
}

async function syncNotion({
  id,
  stage,
  repetition,
  "last review": lastReview,
  "next review": nextReview,
  user: user_id,
  card: card_id,
  "last update": lastUpdate,
  ...props
}) {
  const { user, card } = await getNotionMeta(user_id[0], card_id[0]);

  const notion = await Notions.upsert(
    {
      stage,
      repetition,
      lastReview,
      nextReview,
      cardId: card.id,
      userId: user.id,
    },
    { where: { airtable_id: id } }
  ).catch((err) => {
    console.log(err);
    return null;
  });

  return notion;
}

module.exports.createNotion = createNotion;
module.exports.syncNotion = syncNotion;
