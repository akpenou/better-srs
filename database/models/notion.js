module.exports = (sequelize, DataTypes) => {
  const Notions = sequelize.define(
    "notions",
    {
      airtable_id: DataTypes.STRING,
      stage: DataTypes.STRING,
      repetitions: DataTypes.INTEGER,
      lastReview: DataTypes.DATE,
      nextReview: DataTypes.DATE,
    },
    { underscored: true }
  );

  Notions.associate = (models) => {
    Notions.belongsTo(models.cards);
    Notions.belongsTo(models.users);
  };

  return Notions;
};
