module.exports = (sequelize, DataTypes) => {
  const Sections = sequelize.define(
    "sections",
    {
      airtable_id: DataTypes.STRING,
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
    },
    { underscored: true }
  );

  Sections.associate = (models) => {
    Sections.belongsTo(models.courses);
  };

  return Sections;
};
