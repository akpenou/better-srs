module.exports = (sequelize, DataTypes) => {
  const Events = sequelize.define(
    "events",
    {
      airtable_id: DataTypes.STRING,
      knowledge: DataTypes.STRING,
      experience: DataTypes.INTEGER,
    },
    { underscored: true }
  );

  Events.associate = (models) => {
    Events.belongsTo(models.cards);
    Events.belongsTo(models.users);
  };

  return Events;
};
