const Sequelize = require("sequelize");
const capitalize = require("lodash/capitalize");

const DATABASE_URI =
  "mariadb://admin:Qwertyuiop1!@akpenou-maria.c662mhezyirb.eu-west-3.rds.amazonaws.com:3306/bettersrs";

const sequelize = new Sequelize(DATABASE_URI);

const users = require("./user");
const courses = require("./course");
const cards = require("./card");
const sections = require("./section");
const events = require("./event");
const notions = require("./notion");

const models = [users, courses, cards, sections, events, notions];

const db = {};

models.forEach((builder) => {
  const model = builder(sequelize, Sequelize);
  db[model.name] = model;
});

console.log(db);

const database = {};
Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }

  database[capitalize(modelName)] = db[modelName];
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.error("Unable to connect to the database:", err);
  });

console.log({ database });

module.exports = {
  sequelize,
  ...database,
};
