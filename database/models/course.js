module.exports = (sequelize, DataTypes) => {
  const Courses = sequelize.define(
    "courses",
    {
      airtable_id: DataTypes.STRING,
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
    },
    { underscored: true }
  );

  Courses.associate = (models) => {};

  return Courses;
};
