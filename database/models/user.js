module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    "users",
    {
      airtable_id: DataTypes.STRING,
      name: DataTypes.STRING,
      code: DataTypes.STRING,
      mail: DataTypes.STRING,
      experience: DataTypes.INTEGER,
      level: DataTypes.FLOAT,
      avatar: DataTypes.STRING,
    },
    { underscored: true }
  );

  Users.associate = (models) => {};

  return Users;
};
