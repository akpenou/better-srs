module.exports = (sequelize, DataTypes) => {
  const Cards = sequelize.define(
    "cards",
    {
      airtable_id: DataTypes.STRING,
      question: DataTypes.TEXT,
      answer: DataTypes.TEXT,
    },
    { underscored: true }
  );

  Cards.associate = (models) => {
    Cards.belongsTo(models.sections);
  };

  return Cards;
};
