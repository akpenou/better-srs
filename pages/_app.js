import { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { ThemeProvider } from "styled-components";
import Head from "next/head";

import theme, { globalStyle as GlobalStyle } from "@/ui/theme";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient, { InMemoryCache } from "apollo-boost";

import TopBar from "@/components/NavBar/TopBar";
import BottomBar from "@/components/NavBar/BottomBar";

const Page = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  width: 100vw;
  height: 100vh; /* Fallback for browsers that do not support Custom Properties */
  height: -webkit-fill-available;

  & > * {
    flex-shrink: 0;
  }
`;

const Container = styled.div`
  flex-grow: 2;
  flex-shrink: 1;

  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;

  overflow: auto;

  ${(props) => props.noPadding && `padding: 0px;`}

  box-sizing: border-box;

  background-color: #f4f4f4;
`;

const app = ({ Component, pageProps, host, ...props }) => {
  const isSSR = !process.browser;
  const client = new ApolloClient({
    uri: `/api/graphql`,
    cache: new InMemoryCache(),
    request: (operation) => {
      const token = (!isSSR && localStorage.getItem("token")) || "";

      operation.setContext({
        headers: {
          authorization: token,
        },
      });
    },
  });

  useEffect(() => {
    if (!isSSR) {
      window.scrollTo(0, 1);
    }
  }, [isSSR]);

  return (
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <GlobalStyle />
        <Head>
          <link
            href="https://fonts.googleapis.com/css?family=Poppins@500;700&display=swap"
            rel="stylesheet"
          />
        </Head>
        <Page>
          <TopBar />
          <Container>
            <Component {...pageProps} />
          </Container>
          <BottomBar />
        </Page>
      </ApolloProvider>
    </ThemeProvider>
  );
};

export default app;
