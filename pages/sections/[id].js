import styled from "styled-components";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { sortBy } from "lodash";

import Text from "@/components/Text";

const FETCH_CARDS = gql`
  query fetchCards($sectionId: ID!) {
    section(id: $sectionId) {
      name
    }

    cards(sectionId: $sectionId) {
      id
      question
      answer
    }
  }
`;

const CardList = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data, loading } = useQuery(FETCH_CARDS, {
    variables: { sectionId: id },
  });

  const cards = data && sortBy(data.cards, "question");

  if (!data && loading) {
    return <div>Loading...</div>;
  }

  return (
    <Container>
      <Text size="22px">{data && data.section.name}</Text>
      {cards &&
        cards.map((card) => {
          return (
            <div key={card.id} style={{ width: "100%" }}>
              <Text color="rgba(0, 0, 0, 0.8)" size="12px">
                {card.id}
              </Text>
              <Text bold md={card.question} />
              <Text md={card.answer} />
            </div>
          );
        })}
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding: 20px;
  box-sizing: border-box;

  background-color: #f4f4f4;

  & > *:not(:last-child) {
    margin-bottom: 20px;
  }
`;

export default CardList;
