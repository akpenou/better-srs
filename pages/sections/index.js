import styled from "styled-components";

import Sections from "@/components/Sections";

const Home = () => {
  return (
    <Container>
      <Sections />
    </Container>
  );
};

const Container = styled.div`
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: center;

  padding: 20px;
  box-sizing: border-box;
`;

export default Home;
