import { ApolloServer } from "apollo-server-micro";

import { typeDefs, resolvers } from "@/helpers/_schemas";

import { sequelize } from "../../../database/models";

console.log({ context: { sequelize: sequelize.models } });

const server = new ApolloServer({
  typeDefs,
  resolvers,
  tracing: true,
  context: async ({ req }) => {
    const token = req.headers.authorization;

    if (!token) {
      return { sequelize: sequelize.models };
    }

    const user = await sequelize.models.users.findByPk(token);

    return { user, sequelize: sequelize.models };
  },
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default server.createHandler({ path: "/api/graphql" });
