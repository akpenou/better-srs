import { useState, useEffect } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import Bold from "@/components/Bold";
import Name from "@/components/Profile/Name";
import Level from "@/components/Profile/Level";
import Avatar from "@/components/Profile/Avatar";
import Section from "@/components/Profile/Section";
import KnowledgeStatus from "@/components/Profile/KnowledgeStatus";

const GET_ME = gql`
  query {
    me {
      id
      code
      avatar
      name
      mail
      level
      learning {
        notions {
          stage
        }
        toLearn
        toPractice
        known
      }
    }
  }
`;

const Me = () => {
  const router = useRouter();
  const { data, loading } = useQuery(GET_ME, {
    fetchPolicy: "cache-first",
  });
  const me = data && data.me;

  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("code");
    router.push("/");
  };

  return (
    <Container>
      {me && (
        <>
          <Profile>
            <Avatar src={me.avatar} alt={`${me.name} pic`} />
            <Name>{me.name}</Name>
            <Level>
              <Bold>
                <Color color="rgba(0, 0, 0, 0.8)">niv.</Color>{" "}
                {me.level.toFixed(2)}
              </Bold>
            </Level>
          </Profile>
          <KnowledgeStatus {...me.learning} />
        </>
      )}
      <Section onClick={logout}>Déconnexion</Section>
    </Container>
  );
};

const Color = styled.span`
  font-weight: 500;
  color: ${(props) => props.color || `inherit`};
`;

const Container = styled.div`
  flex-grow: 2;
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  & > *:not(:last-child) {
    margin-bottom: 10px;
  }

  padding: 20px;
  box-sizing: border-box;

  background-color: #f4f4f4;
`;

const Profile = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;

  & > *:not(:last-child) {
    margin-bottom: 5px;
  }

  box-sizing: border-box;
  /* padding: 30px; */
`;

export default Me;
