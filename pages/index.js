import { useState, useEffect } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useLazyQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import Input from "@/components/Input";
import Button from "@/components/Button";

const LOGIN = gql`
  query Login($code: ID!) {
    login(code: $code) {
      id
      code
      name
      mail
      level
    }
  }
`;

const Home = () => {
  const router = useRouter();
  const [login, { data, client }] = useLazyQuery(LOGIN);
  const [code, setCode] = useState("");

  const user = data && data.login;
  const isSSR = !process.browser;

  useEffect(() => {
    if (!isSSR) {
      const code = localStorage.getItem("code");

      setCode(code);
    }
  }, [isSSR]);

  useEffect(() => {
    if (user) {
      console.log({ user });
      const { id, code } = user;

      client.writeData({ data: { id, me: user } });
      localStorage.setItem("token", id);
      localStorage.setItem("code", code);
    }
  }, [data]);

  const onSubmit = () => login({ variables: { code } });

  if (user && code) {
    router.replace("/sections");
  }

  return (
    <Container>
      <Input value={code} onChange={(e) => setCode(e.target.value)} />
      <Button onClick={onSubmit}>Se connecter</Button>
      <Link target="_blank" href="https://airtable.com/shrmEy9QjDxLn74GO">
        Je veux m'inscrire
      </Link>
    </Container>
  );
};

const Container = styled.div`
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  & > *:not(:last-child) {
    margin-bottom: 10px;
  }

  padding: 20px;
  box-sizing: border-box;

  background-color: #f4f4f4;
`;

const Link = styled.a``;

export default Home;
