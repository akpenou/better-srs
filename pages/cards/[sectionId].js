import styled from "styled-components";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import Card from "@/components/Card";

const FETCH_CARDS = gql`
  query fetchCards($sectionId: ID!) {
    cards(sectionId: $sectionId) {
      id
      question
      answer
    }
  }
`;

const CardList = () => {
  const router = useRouter();
  const { sectionId } = router.query;

  return (
    <Container>
      <Card sectionId={sectionId} />
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;

  display: flex;
  align-items: center;
  justify-content: center;

  padding: 20px;
  box-sizing: border-box;

  background-color: #f4f4f4;
`;

export default CardList;
