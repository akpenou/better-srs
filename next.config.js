const path = require("path");

module.exports = {
  webpack(config, options) {
    config.resolve.alias["@/components"] = path.join(__dirname, "components");
    config.resolve.alias["@/ui"] = path.join(__dirname, "ui");
    config.resolve.alias["@/helpers"] = path.join(__dirname, "helpers");
    return config;
  },
};
