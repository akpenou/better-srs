export const getUsers = (sequelize) => () => sequelize.users.findAll();
export const getUser = (sequelize) => (id) => sequelize.users.findByPk(id);
export const getUserByCode = (sequelize) => (code) =>
  sequelize.users.findOne({ where: { code } });
export const addExpToUser = (sequelize) => async (id, exp) => {
  const user = await sequelize.users.findByPk(id);
  console.log({ user, id, exp });

  const experience = (user.experience || 0) + exp;
  const _user = await sequelize.users.update(
    {
      experience,
      level: (25 + Math.sqrt(625 + 100 * experience)) / 50,
    },
    { where: { id: user.id } }
  );

  return _user;
};
