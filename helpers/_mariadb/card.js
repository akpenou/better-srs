export const getCards = (sequelize) => () => sequelize.cards.findAll();
export const getCard = (sequelize) => (id) => sequelize.cards.findByPk(id);
export const getCardsBySectionId = (sequelize) => (sectionId) =>
  sequelize.cards.findAll({ where: { sectionId } });
