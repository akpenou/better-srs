export const qualityToEXP = (quality) => {
  switch (quality) {
    case "GOOD":
      return 10;
    case "MEDIUM":
      return 3;
    case "BAD":
      return 1;
    default:
      return 0;
  }
};

export const createEvent = (sequelize) => (cardId, userId, quality) => {
  const event = sequelize.events.create({
    cardId,
    userId,
    knowledge: quality,
    experience: qualityToEXP(quality),
  });

  return event;
};

export const getEvents = (sequelize) => () => {
  return sequelize.events.findAll();
};
