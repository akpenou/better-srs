import * as userFunctions from "./user";
import * as courseFunctions from "./course";
import * as cardFuctions from "./card";
import * as notionFuctions from "./notion";
import * as eventFuctions from "./event";
import * as sectionFuctions from "./section";

module.exports = {
  ...userFunctions,
  ...courseFunctions,
  ...cardFuctions,
  ...notionFuctions,
  ...eventFuctions,
  ...sectionFuctions,
};
