import { createEvent, qualityToEXP } from "./event";
import { getRepetitionNextDate } from "../_leitner";
import { minmax } from "../_math";
import { max } from "lodash";
import { addExpToUser } from "./user";

export const WEAK = "WEAK";
export const MEDIUM = "MEDIUM";
export const STRONG = "STRONG";

export const getStageLabel = (stage) => {
  const _stage = parseInt(stage);

  if (_stage <= 4) {
    return WEAK;
  } else if (4 < parseInt(stage) && parseInt(stage) <= 8) {
    return MEDIUM;
  } else if (parseInt(stage) > 8) {
    return STRONG;
  }

  return null;
};

export const getNotions = (sequelize) => () => sequelize.notions.findAll();

export const getNotion = (sequelize) => (id) => sequelize.notions.findByPk(id);

export const getNotionsByUserId = (sequelize) => (userId) =>
  sequelize.notions.findAll({
    where: { userId },
  });

export const getNotionsBySectionId = (sequelize) => (sectionId) =>
  sequelize.notions.findAll({ where: { sectionId } });

export const getNotionByCardIdAndUserId = (sequelize) => (cardId, userId) =>
  sequelize.notions.findOne({
    where: {
      cardId: String(cardId),
      userId: String(userId),
    },
  });

export const createNotion = (sequelize) => async (cardId, userId, quality) => {
  const notion = await sequelize.notions.create({
    cardId,
    userId,
    lastReview: Date(),
    nextReview: Date(),
    stage: "1",
    repetition: 1,
  });

  const event = await createEvent(sequelize)(cardId, userId, quality);

  return event;
};

const updateNotionObject = (notion, quality) => {
  const date = new Date();
  const newStage =
    quality === "GOOD" ? minmax(parseInt(notion.stage) + 1, 0, 11) : 1;
  const newRepetitions = max([parseInt(notion.repetitions), 0]) + 1;
  const nextReview = getRepetitionNextDate(newStage, date);

  const newNotion = {
    lastReview: date,
    nextReview: nextReview,
    stage: String(newStage),
    repetitions: newRepetitions,
  };

  return newNotion;
};

export const updateNotion = (sequelize) => async (notionId, quality) => {
  const notion = await getNotion(sequelize)(notionId);

  const _notion = updateNotionObject(notion, quality);
  const userId = notion.userId;
  const cardId = notion.cardId;

  const newNotion = await sequelize.notions.update(_notion, {
    where: { id: notion.id },
  });

  const [user, event] = await Promise.all([
    addExpToUser(sequelize)(userId, qualityToEXP(quality)),
    createEvent(sequelize)(cardId, userId, quality),
  ]);

  return event;
};

export const updateNotionByCardIdAndUserId = (sequelize) => async (
  cardId,
  userId,
  quality
) => {
  const notion = await getNotionByCardIdAndUserId(sequelize)(
    cardId,
    userId
  ).catch((err) => null);

  if (!notion) {
    console.log("create notion");
    const _notion = await createNotion(sequelize)(cardId, userId, quality);

    const [user, event] = await Promise.all([
      addExpToUser(sequelize)(_notion.userId, qualityToEXP(quality)),
      createEvent(sequelize)(_notion.cardId, _notion.userId, quality),
    ]);

    return event;
  }

  console.log({ notion, cardId, userId });

  const _notion = updateNotionObject(notion, quality);
  const _userId = notion.userId;
  const _cardId = notion.cardId;

  const newNotion = await sequelize.notions.update(_notion, {
    where: { id: notion.id },
  });

  const [user, event] = await Promise.all([
    addExpToUser(sequelize)(_userId, qualityToEXP(quality)),
    createEvent(sequelize)(_cardId, _userId, quality),
  ]);

  return event;
};
