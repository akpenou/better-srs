import { base, addId } from "./_initialisation";

const userClean = (user) => {
  let _user = addId(user);
  _user = {
    ..._user,
    avatar: _user.avatar ? _user.avatar[0].thumbnails.large.url : null,
  };

  return _user;
};

export async function getUserByCode(code) {
  const user = await base("users")
    .select({
      filterByFormula: `{code} = "${code}"`,
    })
    .all()
    .then((records) => {
      return userClean(records[0]);
    })
    .catch((err) => console.log({ err }));

  console.log({ user });
  return user;
}

export async function getUsers() {
  const users = await base("users")
    .select()
    .all()
    .then((records) => {
      return records.map(userClean);
    });

  return users;
}

export async function getUser(id) {
  const user = await base("users").find(id).then(userClean);

  return user;
}
