import { base } from "./_initialisation";

export async function getSections() {
  const records = await base("sections")
    .select()
    .all()
    .then((records) => {
      return records.map(({ id, fields }) => {
        return {
          ...fields,
          numberOfCards: fields.cards ? fields.cards.length : 0,
          id,
        };
      });
    });

  return records;
}

export async function getSection(id) {
  const records = await base("sections")
    .find(id)
    .then(({ id, fields }) => {
      return {
        ...fields,
        numberOfCards: fields.cards ? fields.cards.length : 0,
        id,
      };
    });

  return records;
}
