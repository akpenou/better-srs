import { base, addId } from "./_initialisation";

const qualityToEXP = (quality) => {
  switch (quality) {
    case "GOOD":
      return 10;
    case "MEDIUM":
      return 3;
    case "BAD":
      return 1;
    default:
      return 0;
  }
};

export async function createEvent(cardId, userId, quality) {
  const event = await base("events")
    .create([
      {
        fields: {
          card: [cardId],
          user: [userId],
          knowledge: quality,
          experience: qualityToEXP(quality),
        },
      },
    ])
    .then((records) => {
      const record = records.map(addId)[0];

      return record;
    });

  return event;
}

export async function getEvents() {
  const events = await base("events")
    .select()
    .all()
    .then((records) => {
      return records.map(addId);
    })
    .catch((err) => {
      console.log(err);
    });

  console.log("found", events.length, "events");

  return events;
}
