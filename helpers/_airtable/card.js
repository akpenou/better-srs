import { base, addId } from "./_initialisation";

export async function getCards() {
  const records = await base("cards")
    .select()
    .all()
    .then((records) => {
      return records.map(addId);
    });

  return records;
}
