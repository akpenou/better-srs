import { base, addId } from "./_initialisation";

export async function getCourses() {
  const records = await base("courses")
    .select()
    .all()
    .then((records) => {
      return records.map(addId);
    });

  return records;
}

export async function getCourse(id) {
  const records = await base("courses").find(id).then(addId);

  return records;
}
