import { max } from "lodash";

import { getRepetitionNextDate } from "../_leitner";
import { minmax } from "../_math";

import { base, addId } from "./_initialisation";
import { createEvent } from "./event";

const WEAK = "WEAK";
const MEDIUM = "MEDIUM";
const STRONG = "STRONG";

export const getStageLabel = (stage) => {
  const _stage = parseInt(stage);

  if (_stage <= 4) {
    return WEAK;
  } else if (4 < parseInt(stage) && parseInt(stage) <= 8) {
    return MEDIUM;
  } else if (parseInt(stage) > 8) {
    return STRONG;
  }

  return null;
};

const notionClean = (notion) => {
  if (!notion) {
    return notion;
  }

  let _notion = addId(notion);

  _notion = {
    ..._notion,
    lastReview: _notion["last review"],
    nextReview: _notion["next review"],
    stageLabel: getStageLabel(_notion.stage),
  };

  return _notion;
};

export async function getNotions() {
  const notions = await base("notions")
    .select()
    .all()
    .then((records) => {
      return records.map(notionClean);
    });

  return notions;
}

export async function getNotion(notionId) {
  const notion = await base("notions").find(notionId).then(notionClean);

  return notion;
}

export async function getNotionsByUserId(userId) {
  const notion = await base("notions")
    .select({
      filterByFormula: `{user_id} = "${userId}"`,
    })
    .all()
    .then((records) => {
      return records.map(notionClean);
    });

  return notion;
}

export async function getNotionByCardIdAndUserId(cardId, userId) {
  const notion = await base("notions")
    .select({
      filterByFormula: `AND({card_id} = "${cardId}", {user_id} = "${userId}")`,
    })
    .all()
    .then((records) => {
      return notionClean(records[0]);
    });

  return notion;
}

export async function createNotion(cardId, userId, quality) {
  const notion = await base("notions")
    .create([
      {
        fields: {
          card: [cardId],
          user: [userId],
          "last review": "2020-06-08T20:39:00.000Z",
          "next review": "2020-06-08T21:00:00.000Z",
          stage: "1",
          repetitions: 1,
        },
      },
    ])
    .then(notionClean);

  const event = await createEvent(cardId, userId, quality);

  return event;
}

export async function updateNotion(notionId, quality) {
  const notion = await base("notions").find(notionId).then(notionClean);

  const date = new Date();
  const newStage =
    quality === "GOOD" ? minmax(parseInt(notion.stage) + 1, 0, 11) : 1;
  const newRepetitions = max([parseInt(notion.repetitions), 0]) + 1;
  const nextReview = getRepetitionNextDate(newStage, date);
  const userId = notion.user[0];
  const cardId = notion.card[0];

  const newNotion = await base("notions")
    .update([
      {
        id: notionId,
        fields: {
          "last review": date,
          "next review": nextReview,
          stage: String(newStage),
          repetitions: newRepetitions,
        },
      },
    ])
    .then(notionClean);

  const event = await createEvent(cardId, userId, quality);

  return event;
}

export async function updateNotionByCardIdAndUserId(cardId, userId, quality) {
  const notion = await getNotionByCardIdAndUserId(cardId, userId).catch(
    (err) => null
  );

  if (!notion) {
    return createNotion(cardId, userId, quality);
  }

  const date = new Date();
  const newStage =
    quality === "GOOD" ? minmax(parseInt(notion.stage) + 1, 0, 11) : 1;
  const newRepetitions = max([parseInt(notion.repetitions), 0]) + 1;
  const nextReview = getRepetitionNextDate(newStage, date);

  console.log({
    id: notion.id,
    fields: {
      "last review": date,
      "next review": nextReview,
      stage: String(newStage),
      repetitions: newRepetitions,
    },
  });

  const newNotion = await base("notions")
    .update([
      {
        id: notion.id,
        fields: {
          "last review": date,
          "next review": nextReview,
          stage: String(newStage),
          repetitions: newRepetitions,
        },
      },
    ])
    .then(notionClean);

  const event = await createEvent(cardId, userId, quality);

  return event;
}
