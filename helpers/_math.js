import _ from "lodash";

export function minmax(number, min = null, max = null) {
  let newNumber = number;

  if (typeof min === "number") {
    newNumber = _.max([newNumber, min]);
  }

  if (typeof max === "number") {
    newNumber = _.min([newNumber, max]);
  }

  return newNumber;
}
