import { gql } from "apollo-server-micro";

export const typeDef = gql`
  extend type Query {
    events: [Event]
  }

  type Event {
    id: ID
    cardId: ID
    quality: QualityLevel
    experience: Int
  }
`;

export const resolvers = {};
