import { gql } from "apollo-server-micro";
import {
  getNotionByCardIdAndUserId,
  getNotion,
  getNotions,
  createNotion,
  updateNotionByCardIdAndUserId,
} from "@/helpers/_airtable";

export const typeDef = gql`
  enum StageLabel {
    WEAK
    MEDIUM
    STRONG
  }

  extend type Query {
    notion(id: ID, cardId: ID, userId: ID, me: Boolean): Notion
    notions: [Notion]
  }

  extend type Mutation {
    createNotion(
      cardId: ID!
      userId: ID
      quality: QualityLevel
      me: Boolean
    ): Event

    updateNotion(
      cardId: ID!
      userId: ID
      quality: QualityLevel
      me: Boolean
    ): Event
  }

  type Notion {
    id: ID
    card: Card
    user: User
    stage: String
    stageLabel: StageLabel
    lastReview: Date
    nextReview: Date
    repetitions: Int
  }
`;

export const resolvers = {
  Query: {
    notion: async (parent, args, context, info) => {
      if (me) {
        const { user } = context;
        return getNotionByCardIdAndUserId(args.cardId, user.id);
      }

      if (args.cardId && args.userId) {
        return getNotionByCardIdAndUserId(args.cardId, args.userId);
      }

      if (args.id) {
        return getNotion(args.id);
      }

      return null;
    },
    notions: async (parent, args, context, info) => {
      return getNotions();
    },
  },
  Mutation: {
    createNotion: async (parent, args, context, info) => {
      const { cardId, userId, quality, me } = args;

      if (me) {
        const { user } = context;
        return createNotion(cardId, user.id, quality);
      }

      return createNotion(cardId, userId, quality);
    },
    updateNotion: async (parent, args, context, info) => {
      const { cardId, userId, quality, me } = args;

      if (me) {
        const { user } = context;
        return updateNotionByCardIdAndUserId(cardId, user.id, quality);
      }

      return updateNotionByCardIdAndUserId(cardId, userId, quality);
    },
  },
};
