import { gql } from "apollo-server-micro";
import { getCards } from "@/helpers/_airtable";
import _ from "lodash";
import { getNotionByCardIdAndUserId } from "../_airtable/notion";

export const typeDef = gql`
  extend type Query {
    cards(sectionId: ID): [Card]
    card(random: Boolean, sectionId: ID): Card
  }

  type Card {
    id: ID!
    question: String
    answer: String
    notion(me: Boolean): Notion
  }
`;

export const resolvers = {
  Query: {
    card: async (parent, args, context, info) => {
      let cardList = await getCards();

      // console.log({ cardList, card: cardList[0] });
      if (args.sectionId) {
        cardList = cardList.filter(({ section_id }) =>
          section_id.includes(args.sectionId)
        );
      }

      if (args.random) {
        return _.sample(cardList);
      }

      return cardList[0];
    },
    cards: async (parent, args, context, info) => {
      const cardList = await getCards();

      if (args.sectionId) {
        return cardList.filter(({ section_id }) =>
          section_id.includes(args.sectionId)
        );
      }

      return cardList;
    },
  },
  Card: {
    notion: async (parent, args, context) => {
      const { user } = context;
      if (!args.me || !user) {
        return null;
      }

      const notion = await getNotionByCardIdAndUserId(parent.id, user.id);

      return notion;
    },
  },
};
