import { gql } from "apollo-server-micro";
import {
  getUserByCode,
  getUser,
  getUsers,
  getNotionsByUserId,
} from "@/helpers/_airtable";

export const typeDef = gql`
  extend type Query {
    login(code: ID!): User
    users: [User]
    user(id: ID, code: ID): User
    me: User
  }

  type Learning {
    notions: [Notion]
    toLearn: Int
    toPractice: Int
    known: Int
  }

  type User {
    id: ID
    code: ID
    avatar: String
    name: String
    mail: String
    EXP: Int
    LEVEL: Float
    learning: Learning
  }
`;

export const resolvers = {
  Query: {
    login: (parent, args) => {
      return getUserByCode(args.code);
    },
    users: (parent, args) => {
      return getUsers();
    },
    user: (parent, args) => {
      if (args.id) {
        return getUser(args.id);
      }

      if (args.code) {
        return getUserByCode(args.code);
      }

      return null;
    },
    me: (parent, args, context) => {
      const { user } = context;

      return user;
    },
  },
  User: {
    learning: async (parent) => {
      const notions = await getNotionsByUserId(parent.id);
      const toLearn = notions.filter(({ stage }) => parseInt(stage) <= 4)
        .length;
      const toPractice = notions.filter(
        ({ stage }) => 4 < parseInt(stage) && parseInt(stage) <= 8
      ).length;
      const known = notions.filter(({ stage }) => parseInt(stage) > 8).length;

      return { notions, toLearn, toPractice, known };
    },
  },
};
