import { gql } from "apollo-server-micro";
import { getSections, getSection } from "@/helpers/_airtable";

export const typeDef = gql`
  extend type Query {
    sections(limit: Int): [Section]
    section(id: ID!): Section
  }

  type Section {
    id: ID!
    name: String
    description: String
    numberOfCards: Int
  }
`;

export const resolvers = {
  Query: {
    sections: async (parent, args, context, info) => {
      const sectionList = await getSections();

      if (args.limit) {
        return cardList.slice(0, args.limit);
      }

      return sectionList;
    },
    section: async (parent, args, context, info) => {
      const section = await getSection(args.id);

      return section;
    },
  },
};
