import { gql } from "apollo-server-micro";
import {
  getCards,
  getCard,
  getCardsBySectionId,
  getNotionByCardIdAndUserId,
} from "@/helpers/_mariadb";
import _ from "lodash";

export const typeDef = gql`
  extend type Query {
    cards(sectionId: ID): [Card]
    card(random: Boolean, sectionId: ID): Card
  }

  type Card {
    id: ID!
    question: String
    answer: String
    notion(me: Boolean, userId: ID): Notion
  }
`;

export const resolvers = {
  Query: {
    card: async (parent, args, { sequelize }, info) => {
      let cardList = [];

      if (args.sectionId) {
        cardList = await getCardsBySectionId(sequelize)(args.sectionId);
      } else {
        cardList = await getCards(sequelize)();
      }

      if (args.random) {
        return _.sample(cardList);
      }

      return cardList[0];
    },
    cards: async (parent, args, { sequelize }, info) => {
      if (args.sectionId) {
        return getCardsBySectionId(sequelize)(args.sectionId);
      }

      return getCards(sequelize)();
    },
  },
  Card: {
    notion: async (parent, args, { sequelize, user }) => {
      if (!args.userId && (!args.me || !user)) {
        return null;
      }

      if (args.me && user) {
        console.log({ cardId: parent.id, userId: user.id });
        return getNotionByCardIdAndUserId(sequelize)(parent.id, user.id);
      }

      const notion = await getNotionByCardIdAndUserId(sequelize)(
        parent.id,
        args.userId
      );

      console.log({ notion, userId: args.userId, cardId: parent.id });

      return notion;
    },
  },
};
