import { gql } from "apollo-server-micro";
import { GraphQLScalarType } from "graphql";
import { Kind } from "graphql/language";
import { merge } from "lodash";
import moment from "moment";

import { typeDef as Card, resolvers as cardResolvers } from "./card";
import { typeDef as Event, resolvers as eventResolvers } from "./event";
import { typeDef as Notion, resolvers as notionResolvers } from "./notion";
import { typeDef as Section, resolvers as sectionResolvers } from "./section";
import { typeDef as User, resolvers as userResolvers } from "./user";

const _typeDefs = gql`
  scalar Date

  enum QualityLevel {
    BAD
    MEDIUM
    GOOD
  }

  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }
`;

const _resolvers = {
  Date: new GraphQLScalarType({
    name: "Date",
    description: "Date custom scalar type",
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return moment(value).toISOString(); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    },
  }),
};

export const typeDefs = [_typeDefs, Card, Event, Notion, Section, User];
export const resolvers = merge(
  _resolvers,
  cardResolvers,
  eventResolvers,
  notionResolvers,
  sectionResolvers,
  userResolvers
);
