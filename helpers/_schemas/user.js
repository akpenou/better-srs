import { gql } from "apollo-server-micro";
import {
  getUserByCode,
  getUser,
  getUsers,
  getNotionsByUserId,
} from "@/helpers/_mariadb";

export const typeDef = gql`
  extend type Query {
    login(code: ID!): User
    users: [User]
    user(id: ID, code: ID): User
    me: User
  }

  type Learning {
    notions: [Notion]
    toLearn: Int
    toPractice: Int
    known: Int
  }

  type User {
    id: ID
    code: ID
    avatar: String
    name: String
    mail: String
    experience: Int
    level: Float
    learning: Learning
  }
`;

export const resolvers = {
  Query: {
    login: (parent, args, { sequelize }) => {
      return getUserByCode(sequelize)(args.code);
    },
    users: (parent, args, { sequelize }) => {
      return getUsers(sequelize)();
    },
    user: (parent, args, { sequelize }) => {
      if (args.id) {
        return getUser(sequelize)(args.id);
      }

      if (args.code) {
        return getUserByCode(sequelize)(args.code);
      }

      return null;
    },
    me: (parent, args, { user }) => {
      return user;
    },
  },
  User: {
    learning: async (parent, _, { sequelize }) => {
      const notions = await getNotionsByUserId(sequelize)(parent.id);

      const toLearn = notions.filter(({ stage }) => parseInt(stage) <= 4)
        .length;
      const toPractice = notions.filter(
        ({ stage }) => 4 < parseInt(stage) && parseInt(stage) <= 8
      ).length;
      const known = notions.filter(({ stage }) => parseInt(stage) > 8).length;

      return { notions, toLearn, toPractice, known };
    },
  },
};
