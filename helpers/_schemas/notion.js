import { gql } from "apollo-server-micro";
import {
  getNotionByCardIdAndUserId,
  getNotion,
  getNotions,
  createNotion,
  updateNotionByCardIdAndUserId,
  getCard,
  getUser,
  getStageLabel,
} from "@/helpers/_mariadb";

export const typeDef = gql`
  enum StageLabel {
    WEAK
    MEDIUM
    STRONG
  }

  extend type Query {
    notion(id: ID, cardId: ID, userId: ID, me: Boolean): Notion
    notions: [Notion]
  }

  extend type Mutation {
    createNotion(
      cardId: ID!
      userId: ID
      quality: QualityLevel
      me: Boolean
    ): Event

    updateNotion(
      cardId: ID!
      userId: ID
      quality: QualityLevel
      me: Boolean
    ): Event
  }

  type Notion {
    id: ID
    card: Card
    user: User
    stage: String
    stageLabel: StageLabel
    lastReview: Date
    nextReview: Date
    repetitions: Int
  }
`;

export const resolvers = {
  Query: {
    notion: async (parent, args, { sequelize, user }, info) => {
      if (args.me) {
        return getNotionByCardIdAndUserId(sequelize)(args.cardId, user.id);
      }

      if (args.cardId && args.userId) {
        return getNotionByCardIdAndUserId(sequelize)(args.cardId, args.userId);
      }

      if (args.id) {
        return getNotion(sequelize)(args.id);
      }

      return null;
    },
    notions: async (parent, args, { sequelize }, info) => {
      return getNotions(sequelize)();
    },
  },
  Mutation: {
    createNotion: async (
      parent,
      { cardId, userId, quality, me },
      { user },
      info
    ) => {
      if (me) {
        return createNotion(sequelize)(cardId, user.id, quality);
      }

      return createNotion(sequelize)(cardId, userId, quality);
    },
    updateNotion: async (parent, args, { sequelize, user }, info) => {
      const { cardId, userId, quality, me } = args;

      if (me) {
        return updateNotionByCardIdAndUserId(sequelize)(
          cardId,
          user.id,
          quality
        );
      }

      return updateNotionByCardIdAndUserId(sequelize)(cardId, userId, quality);
    },
  },
  Notion: {
    card: async (parent, args, { sequelize }, info) => {
      return getCard(sequelize)(parent.cardId);
    },
    user: async (parent, args, { sequelize }, info) => {
      return getUser(sequelize)(parent.userId);
    },
    stageLabel: (parent, args, { sequelize }, info) => {
      return getStageLabel(parent.stage);
    },
  },
};
