import { gql } from "apollo-server-micro";
import {
  getSections,
  getSection,
  getCardsBySectionId,
} from "@/helpers/_mariadb";

export const typeDef = gql`
  extend type Query {
    sections(limit: Int): [Section]
    section(id: ID!): Section
  }

  type Section {
    id: ID!
    name: String
    description: String
    numberOfCards: Int
    cards: [Card]
  }
`;

export const resolvers = {
  Query: {
    sections: async (parent, args, { sequelize }, info) => {
      const sectionList = await getSections(sequelize)();

      if (args.limit) {
        return sectionList.slice(0, args.limit);
      }

      return sectionList;
    },
    section: async (parent, args, { sequelize }, info) => {
      const section = await getSection(sequelize)(args.id);

      return section;
    },
  },
  Section: {
    numberOfCards: async (parent, args, { sequelize }, info) => {
      const cards = await getCardsBySectionId(sequelize)(parent.id);

      return cards ? cards.length : 0;
    },
    cards: async (parent, args, { sequelize }, info) => {
      const cards = await getCardsBySectionId(sequelize)(parent.id);

      return cards;
    },
  },
};
