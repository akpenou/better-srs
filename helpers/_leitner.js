import moment from "moment";

export function getRepetitionNextDate(stage, date) {
  switch (stage) {
    case 1:
      return moment(date).add(5, "s").toISOString();
    case 2:
      return moment(date).add(25, "s").toISOString();
    case 3:
      return moment(date).add(2, "m").toISOString();
    case 4:
      return moment(date).add(10, "m").toISOString();
    case 5:
      return moment(date).add(1, "h").toISOString();
    case 6:
      return moment(date).add(5, "h").toISOString();
    case 7:
      return moment(date).add(1, "d").toISOString();
    case 8:
      return moment(date).add(5, "d").toISOString();
    case 9:
      return moment(date).add(25, "d").toISOString();
    case 10:
      return moment(date).add(4, "M").toISOString();
    case 11:
      return moment(date).add(2, "Y").toISOString();
    default:
      throw new Error("The stage is not valid");
  }
}
