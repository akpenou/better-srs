import styled, { css } from "styled-components";

export const ERROR = "ERROR";
export const WARNING = "WARNING";
export const SUCCESS = "SUCCESS";

const getBgColor = (state) => {
  switch (state) {
    case ERROR:
      return "#f3d9d9";
    case WARNING:
      return "#f3ecd9";
    case SUCCESS:
      return "#d9ecdf";
    default:
      return "none";
  }
};

export const getColor = (state) => {
  switch (state) {
    case ERROR:
      return "#B30000";
    case WARNING:
      return "#B18001";
    case SUCCESS:
      return "#00802B";
    default:
      return "none";
  }
};

const Tag = styled.span`
  font-style: normal;
  font-weight: 900;
  font-size: 18px;
  line-height: 21px;

  box-sizing: border-box;
  padding: 5px 10px;
  border-radius: 5px;

  ${(props) =>
    props.state &&
    css`
      background-color: ${getBgColor(props.state)};
      color: ${getColor(props.state)};
    `}
`;

export default Tag;
