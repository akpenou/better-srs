import React from "react";
import styled from "styled-components";
import Link from "next/link";

const NavBar = () => {
  return (
    <Container>
      <Link href="/">
        <Text>🏠 Better SRS </Text>
      </Link>
    </Container>
  );
};

const Container = styled.nav`
  width: 100%;
  height: 60px;
  position: fixed;

  background-color: #fbf7f5;

  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.h1`
  margin: 0;

  font-weight: bold;
  font-size: 26px;

  text-decoration: none;
`;

export default NavBar;
