import React from "react";
import styled, { css } from "styled-components";
import Link from "next/link";

const ButtonComponent = ({ emoji, text, href, ...props }) => {
  return (
    <Link href={href}>
      <Button {...props}>
        <Text emoji>{emoji}</Text>
        <Text>{text}</Text>
      </Button>
    </Link>
  );
};

const BottomBar = () => {
  return (
    <Container>
      <ButtonComponent text="sections" emoji="✍️" href="/sections" />
      <ButtonComponent text="profile" emoji="👨‍💻" href="/me" />
    </Container>
  );
};

const Container = styled.nav`
  width: 100%;
  height: 60px;

  background-color: #fbf7f5;

  display: flex;
  align-items: center;
  justify-content: space-evenly;
`;

const Button = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Text = styled.h1`
  margin: 0;

  font-weight: bold;
  font-size: 14px;

  ${(props) =>
    props.emoji &&
    css`
      font-size: 28px;
    `}

  text-decoration: none;
`;

export default BottomBar;
