import React from "react";
import styled, { css } from "styled-components";

const Button = (props) => {
  return <ButtonComponent {...props} />;
};

const ButtonComponent = styled.button`
  border: none;

  &:focus {
    outline: none;
  }

  border-radius: 5px;
  padding: 10px 20px;

  background: #0047ff;

  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  display: flex;
  align-items: center;
  text-align: center;

  color: #ffffff;

  ${(props) =>
    props.emoji &&
    css`
      background: none;
      font-size: 36px;
      padding: 5px;
    `}
`;

export default Button;
