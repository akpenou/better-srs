import styled from "styled-components";

const FlexBox = styled.div`
  display: flex;
  ${(props) =>
    [props.spaceBetween && "justify-content: space-between;"].join("\n")}
`;

export default FlexBox;
