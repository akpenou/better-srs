import styled from "styled-components";

const Level = styled.h2`
  margin: 0;

  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;

  /* identical to box height */
  display: flex;
  align-items: center;

  color: #000000;
`;

export default Level;
