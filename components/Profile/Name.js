import styled from "styled-components";

const Name = styled.h2`
  margin: 0;

  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 36px;

  /* identical to box height */
  display: flex;
  align-items: center;

  color: #000000;
`;

export default Name;
