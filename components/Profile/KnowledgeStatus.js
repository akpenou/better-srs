import { useRef, useEffect } from "react";
import styled from "styled-components";
import Chart from "chart.js";
import { groupBy, map, times } from "lodash";

import Section from "@/components/Profile/Section";

const RED = "#B30000";
const YELLOW = "#B18001";
const GREEN = "#00802B";

const stageToColor = (stage) => {
  if (parseInt(stage) <= 4) {
    return RED;
  }

  if (4 < parseInt(stage) && parseInt(stage) <= 8) {
    return YELLOW;
  }

  return GREEN;
};

const NotionStatusBlock = ({ notions, toLearn, toPractice, known }) => {
  const ref = useRef();

  useEffect(() => {
    const ctx = ref.current && ref.current.getContext("2d");

    if (!ctx) {
      console.log("kill");
      return () => null;
    }

    const stages = groupBy(map(notions, "stage"));
    const data = times(11).map((index) => {
      const id = index + 1;

      return {
        stage: id,
        count: (stages[id] || []).length,
      };
    });

    new Chart(ctx, {
      type: "bar",
      data: {
        //Bring in data
        labels: map(data, "stage"),
        datasets: [
          {
            label: "Stages",
            data: map(data, "count"),
            backgroundColor: times(11).map((idx) => stageToColor(idx + 1)),
          },
        ],
      },
      options: {
        //Customize chart options
      },
    });
  }, [notions, ref.current]);

  return (
    <Container>
      <Section>Notions</Section>
      <Canvas ref={ref} />

      <NotionStatusContainer>
        <NotionStatus name="apprendre" counter={toLearn} color={RED} />
        <NotionStatus name="à reviser" counter={toPractice} color={YELLOW} />
        <NotionStatus name="connues" counter={known} color={GREEN} />
      </NotionStatusContainer>
    </Container>
  );
};

const NotionStatus = ({ name, counter, color, ...props }) => {
  return (
    <NotionStatusItem color={color} {...props}>
      <Counter>{counter}</Counter>
      <Name>{name}</Name>
    </NotionStatusItem>
  );
};

const NotionStatusItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  color: ${(props) => props.color || `inherit`};
`;

const NotionStatusContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;

  max-width: 300px;
  width: 100%;
`;

const Counter = styled.h3`
  margin: 0;

  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;

  /* identical to box height */
  display: flex;
  align-items: center;
`;

const Name = styled.h2`
  margin: 0;

  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;

  /* identical to box height */
  display: flex;
  align-items: center;
`;

const Container = styled.div`
  width: 100%;
  height: 100%;

  flex-grow: 2;

  display: flex;
  flex-direction: column;
  align-items: center;

  & > *:not(:last-child) {
    margin-bottom: 5px;
  }

  box-sizing: border-box;
`;

const Canvas = styled.canvas`
  width: 100%;
  max-width: 500px;
`;

export default NotionStatusBlock;
