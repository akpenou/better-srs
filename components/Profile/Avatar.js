import styled from "styled-components";

const AvatarComponent = (props) => {
  return <Avatar {...props} />;
};

const Avatar = styled.img`
  height: 75px;
  width: 75px;
  border-radius: 50%;

  background-color: grey;
  border: 2px solid white;
  object-fit: cover;
`;

export default AvatarComponent;
