import styled from "styled-components";

const Section = styled.h2`
  margin: 0;

  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  display: flex;
  align-items: center;
  text-align: center;

  color: rgba(0, 0, 0, 0.8);
`;

export default Section;
