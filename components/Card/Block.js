import React from "react";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";

const Block = ({ children, text, md, ...props }) => {
  if (md) {
    return (
      <Container>
        <ReactMarkdown source={md} />
      </Container>
    );
  }
  return <Container {...props}>{children || text}</Container>;
};

const Container = styled.div`
  width: 100%;
  max-width: 500px;
  min-height: 200px;

  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  text-align: center;

  color: #000000;

  background: #fff8f3;

  border-radius: 5px;

  padding: 20px;
  box-sizing: border-box;

  overflow-y: auto;

  & pre {
    width: 100%;
    display: flex;
    text-align: left;
  }
`;

export default Block;
