import React, { useState, useEffect } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import Question from "./Stages/Question";
import Answer from "./Stages/Answer";
import Experience from "./Stages/Experience";

const GET_CARD = gql`
  query FetchCard($sectionId: ID) {
    card(random: true, sectionId: $sectionId) {
      id
      question
      answer
      notion(me: true) {
        id
        lastReview
        nextReview
        stage
        stageLabel
        repetitions
      }
    }
  }
`;

const UPDATE_NOTION = gql`
  mutation updateNotion($cardId: ID!, $quality: QualityLevel!) {
    updateNotion(cardId: $cardId, me: true, quality: $quality) {
      id
      cardId
      quality
      experience
    }
  }
`;

const ASK = "ASK";
const ANSWER = "ANSWER";
const EXP = "EXP";

const stages = [ASK, ANSWER, EXP];

const Card = ({ sectionId, ...props }) => {
  const { loading, data, refetch } = useQuery(GET_CARD, {
    variables: { sectionId },
  });
  const [updateNotion, { data: event, error }] = useMutation(UPDATE_NOTION);
  const [stageIdx, setStageIdx] = useState(0);

  const nextStage = () => setStageIdx((stageIdx + 1) % stages.length);
  const stage = stages[stageIdx];

  if (loading) return <p>Loading ...</p>;

  if (stage === ASK) {
    const onNextStage = () => {
      nextStage();
    };

    return (
      <Question
        question={data && data.card.question}
        stageLabel={data && data.card.notion && data.card.notion.stageLabel}
        nextReview={data && data.card.notion && data.card.notion.nextReview}
        nextStage={onNextStage}
        refetch={() => refetch()}
      />
    );
  }

  if (stage === ANSWER) {
    const onNextStage = (quality) => {
      console.log({
        variables: {
          cardId: data.card.id,
          quality,
        },
      });
      updateNotion({
        variables: {
          cardId: data.card.id,
          quality,
        },
      });

      refetch();
      nextStage();
    };

    return <Answer answer={data && data.card.answer} nextStage={onNextStage} />;
  }

  if (stage === EXP) {
    const onNextStage = () => {
      nextStage();
    };

    return (
      <Experience
        exp={event && event.updateNotion.experience}
        nextStage={onNextStage}
      />
    );
  }

  return <div {...props}>Sorry we have bugged...</div>;
};

export default Card;
