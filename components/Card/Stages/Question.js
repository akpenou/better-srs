import React from "react";
import styled from "styled-components";
import moment from "moment";

import Block from "../Block";
import Button from "../Button";
import Tag, { ERROR, WARNING, SUCCESS } from "@/components/Tag";

const stageLabelToState = {
  WEAK: ERROR,
  MEDIUM: WARNING,
  STRONG: SUCCESS,
};

const QuestionComponent = ({
  nextStage,
  question,
  stageLabel,
  refetch,
  nextReview,
  ...props
}) => {
  const isPracticable = moment(nextReview).isBefore(moment());

  return (
    <Container {...props}>
      <Tag state={stageLabelToState[stageLabel]}>{stageLabel}</Tag>
      {nextReview &&
        (isPracticable ? (
          <Tag state={ERROR}>
            à reviser depuis{" "}
            {moment(moment().toISOString()).diff(nextReview, "d")} jours
          </Tag>
        ) : (
          <Tag state={SUCCESS}>
            à reviser dans {moment(nextReview).diff(moment(), "d")} jours
          </Tag>
        ))}
      <Block text={question} />
      <Button onClick={nextStage}>Voir la réponse</Button>
      <Link onClick={refetch}>skip</Link>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  max-width: 400px;

  display: flex;
  flex-direction: column;
  align-items: center;

  & > *:not(:last-child) {
    margin-bottom: 20px;
  }
`;

const Link = styled.a`
  cursor: pointer;
  text-decoration: underline;
`;

export default QuestionComponent;
