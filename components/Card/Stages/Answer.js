import React from "react";
import styled from "styled-components";

import Text from "@/components/Text";
import Button from "../Button";
import Block from "../Block";

const GOOD = "GOOD";
const MEDIUM = "MEDIUM";
const BAD = "BAD";

const Answer = ({ nextStage, answer, ...props }) => {
  return (
    <Container {...props}>
      <Block md={answer} />
      <QualityBlock>
        <Text>Ma connaissance était...</Text>
        <AsnwerQuality>
          <Button emoji onClick={() => nextStage(GOOD)}>
            😊
          </Button>
          <Button emoji onClick={() => nextStage(MEDIUM)}>
            😐
          </Button>
          <Button emoji onClick={() => nextStage(BAD)}>
            🙁
          </Button>
        </AsnwerQuality>
      </QualityBlock>
    </Container>
  );
};

const QualityBlock = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Container = styled.div`
  width: 100%;
  max-width: 400px;

  display: flex;
  flex-direction: column;
  align-items: center;

  & > *:not(:last-child) {
    margin-bottom: 20px;
  }
`;

const AsnwerQuality = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-evenly;
`;

export default Answer;
