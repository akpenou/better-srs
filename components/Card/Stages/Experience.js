import React from "react";
import styled from "styled-components";

import Text from "@/components/Text";
import Button from "../Button";

const ExperienceComponent = ({ nextStage, exp, ...props }) => {
  return (
    <Container {...props}>
      <Text text={`Vous avez reçu ${exp} points d'expérience`} />
      <Button onClick={nextStage}>Nouvelle carte</Button>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  max-width: 400px;

  display: flex;
  flex-direction: column;
  align-items: center;

  & > *:not(:last-child) {
    margin-bottom: 20px;
  }
`;

export default ExperienceComponent;
