import styled from "styled-components";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import Link from "next/link";
import _ from "lodash";

import SectionItem from "./SectionItem";
import { getStageLabel } from "../../helpers/_airtable/notion";

const GET_SECTIONS = gql`
  query {
    sections {
      id
      name
      numberOfCards
      cards {
        id
        question
        notion(me: true) {
          id
          stage
          repetitions
        }
      }
    }
  }
`;

const enhanceSectionsMapper = (section) => {
  const notions = _.map(section.cards, "notion");

  const seenCount = _.filter(notions).length;
  const seenPercentage = seenCount / notions.length;
  const stageAvg = _.meanBy(_.filter(notions), (notion) =>
    notion ? parseInt(notion.stage, 10) : 0
  );

  const stageLabel = getStageLabel(stageAvg);

  return { ...section, seenCount, seenPercentage, stageAvg, stageLabel };
};

const Sections = ({ ...props }) => {
  const { loading, data } = useQuery(GET_SECTIONS, {
    fetchPolicy: "cache-and-network",
  });

  if (!data && loading) return <p>Loading ...</p>;

  const sections =
    data &&
    _.sortBy(
      data.sections.map(enhanceSectionsMapper),
      ({ seenPercentage, stageAvg }) => seenPercentage * stageAvg
    );

  console.log({ sections });

  return (
    <Container {...props}>
      {sections &&
        sections.map((section) => {
          return <SectionItem {...section} clickable key={section.id} />;
        })}
    </Container>
  );
};

const Container = styled.div`
  height: 100%;
  width: 100%;
  max-width: 500px;

  & > *:not(:last-child) {
    margin-bottom: 20px;
  }
`;

const Title = styled.h1`
  margin: 0;
  height: 40px;

  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 36px;
  display: flex;
  align-items: center;
  text-align: center;

  color: rgba(0, 0, 0, 0.8);
`;

export default Sections;
