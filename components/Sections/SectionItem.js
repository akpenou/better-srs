import React from "react";
import styled, { css } from "styled-components";
import FlexBox from "@/components/FlexBox";
import Text, {
  SUCCESS as SUCCESS_TEXT,
  WARNING as WARNING_TEXT,
} from "@/components/Text";
import Link from "next/link";
import Tag, { ERROR, WARNING, SUCCESS } from "@/components/Tag";

const stageLabelToState = {
  WEAK: ERROR,
  MEDIUM: WARNING,
  STRONG: SUCCESS,
};

const SectionItem = ({
  id,
  name,
  numberOfCards,
  seenCount,
  stageLabel,
  clickable,
  ...props
}) => {
  return (
    <Container clickable={clickable} {...props}>
      <Link href={`/cards/${id}`}>
        <Name>{name}</Name>
      </Link>
      <FlexBox spaceBetween>
        <Actions>
          <Link href={`/cards/${id}`}>
            <Text color={SUCCESS_TEXT}>Apprendre</Text>
          </Link>
          <Link href={`/sections/${id}`}>
            <Text color={WARNING_TEXT}>Voir les cartes</Text>
          </Link>
        </Actions>

        <Counter>
          <Tag state={stageLabelToState[stageLabel]}>
            {seenCount} / {numberOfCards} cartes
          </Tag>
        </Counter>
      </FlexBox>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;

  ${(props) =>
    props.clickable &&
    css`
      cursor: pointer;
    `}
`;

const Name = styled.h2`
  margin: 0;
  flex-grow: 1;

  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;

  /* identical to box height */
  display: flex;
  align-items: center;

  color: #000000;
`;

const Counter = styled.h3`
  align-self: flex-end;
  flex: 0 0 auto;
  margin: 0;

  width: fit-content;

  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 27px;

  color: rgba(0, 0, 0, 0.8);
`;

const Actions = styled.div`
  display: flex;

  & > *:not(:last-child) {
    margin-right: 5px;
  }
`;

export default SectionItem;
