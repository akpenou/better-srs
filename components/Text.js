import React from "react";
import styled, { css } from "styled-components";
import ReactMarkdown from "react-markdown";

export const ERROR = "ERROR";
export const WARNING = "WARNING";
export const SUCCESS = "SUCCESS";

const getColor = (state) => {
  switch (state) {
    case ERROR:
      return "#B30000";
    case WARNING:
      return "#B18001";
    case SUCCESS:
      return "#00802B";
    default:
      return state;
  }
};

const Text = ({ children, text, color, bold, md, ...props }) => {
  return (
    <Container color={color} bold={bold} {...props}>
      {md ? <ReactMarkdown source={md} /> : children || text}
    </Container>
  );
};

const Container = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 1.3;

  color: #000000;
  ${(props) =>
    props.color &&
    css`
      color: ${getColor(props.color)};
    `}

  ${(props) =>
    props.bold &&
    css`
      font-weight: bold;
    `}

  ${(props) =>
    props.size &&
    css`
      font-size: ${props.size};
    `}

  & p {
    margin: 0;
  }
`;

export default Text;
