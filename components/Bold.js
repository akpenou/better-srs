import styled from "styled-components";

const Bold = styled.strong`
  font-weight: bold;
`;

export default Bold;
