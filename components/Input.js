import React from "react";
import styled, { css } from "styled-components";

const Input = (props) => {
  return <InputComponent {...props} />;
};

const InputComponent = styled.input`
  border: 2px solid grey;

  &:focus {
    outline: none;
    border: 2px solid #0047ff;
  }

  border-radius: 5px;
  padding: 10px 20px;

  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  display: flex;
  align-items: center;
  text-align: center;
`;

export default Input;
