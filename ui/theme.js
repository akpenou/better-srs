import { css, createGlobalStyle } from "styled-components";

export const globalStyle = createGlobalStyle`  
  body {
    margin: 0;
    font-family: Poppins, -apple-system, BlinkMacSystemFont, Avenir Next, Avenir, Helvetica, sans-serif;
    background-color: #FBF7F5;

    & > #__next {
      min-width: 100vw;
      min-height: 100vh;
      min-height: -webkit-fill-available;
    }
  }

  html {
    height: -webkit-fill-available;
  }
`;

const theme = {
  func: {
    childGap: (gap = 10) => {
      return css`
        & > *:not(:last-child) {
          margin-bottom: ${gap}px;
        }
      `;
    },
  },
  border: {
    radius: css`
      border-radius: 5px;
    `,
  },
  colors: {
    background: "#FBF7F5",
    wallpaper: "#FBF7F5",
    text: "rgba(0, 0, 0, 0.8)",
    primary: "#F46A54",
    secondary: "#00BE8D",
    error: "#B00707",
    success: "#0D6530",
  },
  fontSizes: {
    small: 12,
    normal: 16,
    large: 24,
    huge: 36,
  },
  text: {
    title: css``,
    caption: css``,
    normal: css``,
    smallText: css``,
    link: css``,
    smallLink: css``,
    label: css``,
    error: css``,
    success: css``,
    section: css``,
  },
  shadow: css`
    box-shadow: rgba(0, 0, 0, 0.2) 0px 2px 1px -1px,
      rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 1px 3px 0px;
  `,
};

export default theme;
